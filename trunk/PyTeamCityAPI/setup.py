from distutils.core import setup

setup(
	name = 'PyTeamCityAPI',
	version = '1.0.0',
	py_modules = ['api'],
	author = 'c0x3y',
	author_email = 'jcoxey@gmail.com',
	url = 'https://bitbucket.org/c0x3y/pyteamcityapi',
	description = 'Connector that allows you to connect to the TeamCity REST API'
)