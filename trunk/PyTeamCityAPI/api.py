import urllib2
import urllib 
import base64
import re
from xml.dom.minidom import parse, parseString
from time import strptime, strftime
from xml.dom.minidom import parse, parseString

def ConvertXmlToDictionary(document, node):
	doc = parseString(document)
	nodes = doc.getElementsByTagName(node)
	result = []
	for child in nodes:
		itemToAdd = {}
		for attributeName, attributeValue in child.attributes.items():
			itemToAdd[attributeName] = attributeValue
		result.append(itemToAdd)
	return result

class api:
	url = None
	username = None
	password = None
	def __init__(self, url, username = None, password = None):
		self.username = username
		self.url = url
		self.password = password

	# Only supports dates in the format YYYY-MM-DD
	def FormatDate(self, date):
		actualDate = strptime(date, '%Y-%m-%d')
		return strftime('%Y%m%dT%H%M%S+0000', actualDate)

	# Get the response from the given url
	def GetResponse(self, uri):
		uri = self.url + uri
		print 'Requesting', uri

		passwordManager = urllib2.HTTPPasswordMgrWithDefaultRealm()
		passwordManager.add_password(None, uri, self.username,self.password)
		authenticationHandler = urllib2.HTTPBasicAuthHandler(passwordManager)
		opener = urllib2.build_opener(authenticationHandler)
		urllib2.install_opener(opener)

		response = urllib2.urlopen(uri).read()

		return response
	def GetBuildErrors(self, lastRunDate):
		requestUrl = "/httpAuth/app/rest/builds/?locator=status:FAILURE,sinceDate:" + urllib.quote(lastRunDate)
		errors = ConvertXmlToDictionary(self.GetResponse(requestUrl), 'build');
		for index in range(len(errors)):
			buildId = re.search("(?<=[:])(.*)", errors[index]['href']).group(0)				
			errors[index]['buildId']  = buildId
		return errors

	def GetChangeList(self, buildId):
		requestUrl = "httpAuth/app/rest/changes?build=id:" + str(buildId)
		return ConvertXmlToDictionary(self.GetResponse(requestUrl), 'change')
		
	def GetChangeDetails(self, changeId):
		requestUrl = "httpAuth/app/rest/changes/id:" + str(changeId)
		return ConvertXmlToDictionary(self.GetResponse(requestUrl), 'change')

	def GetUsers(self):
		requestUrl = "httpAuth/app/rest/users"

		response = self.GetResponse(requestUrl)
		return ConvertXmlToDictionary(response,"user")